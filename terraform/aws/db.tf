module "qwerty_db" {
  source = "./db"

  name                                  = var.qwerty_db_name
  engine                                = var.qwerty_db_engine
  engine_version                        = var.qwerty_db_engine_version
  instance_class                        = var.qwerty_db_instance_class
  storage                               = var.qwerty_db_storage
  user                                  = var.qwerty_db_user
  password                              = var.qwerty_db_password
  random_password                       = var.qwerty_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.qwerty_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
